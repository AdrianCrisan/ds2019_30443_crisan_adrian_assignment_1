package com.ds.lab.repository;

import com.ds.lab.model.Patient;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface PatientRepository extends UserRepository<Patient> {

    Patient findByUsername(String username);

    void deleteByUsername(String username);

    @Query(value = "SELECT * FROM Patient p WHERE p.caretaker_id=?1", nativeQuery = true)
    List<Patient> findAllPatientsOfCaretaker(Long caretaker_id);

    @Query(value = "SELECT * FROM Patient p WHERE doctor_id=?1", nativeQuery = true)
    List<Patient> findAllPatientsOfDoctor(Long doctor_id);
}

package com.ds.lab;

import com.ds.lab.model.*;
import com.ds.lab.repository.CaretakerRepository;
import com.ds.lab.repository.DoctorRepository;
import com.ds.lab.repository.PatientRepository;
import com.ds.lab.service.CaretakerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private CaretakerRepository caretakerRepository;

    @Autowired
    private CaretakerService caretakerService;

    @Override
    public void run(String... args) throws Exception {

//        String date = "2019-10-03";
//
//        Doctor d1 = new Doctor();
//        d1.setName("doctor1");
//        d1.setPassword("doctor1");
//        d1.setUsername("doctor1");
//        d1.setAddress("address");
//        d1.setBirthDate(Date.valueOf(date));
//        d1.setGender(Gender.GENDER_M);
//        d1.setRole(RoleName.ROLE_DOCTOR);
//
//        Caretaker c1 = new Caretaker();
//        c1.setDoctor(d1);
//        c1.setAddress("address");
//        c1.setBirthDate(Date.valueOf(date));
//        c1.setGender(Gender.GENDER_M);
//        c1.setName("caretaker1");
//        c1.setPassword("caretaker1");
//        c1.setUsername("caretaker1");
//        c1.setRole(RoleName.ROLE_CARETAKER);
//
//        Caretaker c2 = new Caretaker();
//        c2.setDoctor(d1);
//        c2.setUsername("caretaker2");
//        c2.setName("caretaker2");
//        c2.setPassword("caretaker2");
//        c2.setGender(Gender.GENDER_F);
//        c2.setRole(RoleName.ROLE_CARETAKER);
//        c2.setAddress("address");
//        c2.setBirthDate(Date.valueOf(date));
//
//        d1.addCaretaker(c1);
//        d1.addCaretaker(c2);
//
//        Patient p1 = new Patient();
//        p1.setDoctor(d1);
//        p1.setCaretaker(c1);
//        p1.setAddress("address");
//        p1.setBirthDate(Date.valueOf(date));
//        p1.setGender(Gender.GENDER_F);
//        p1.setName("patient1");
//        p1.setPassword("patient1");
//        p1.setUsername("patient1");
//        p1.setRole(RoleName.ROLE_PATIENT);
//
//        Patient p2 = new Patient();
//        p2.setDoctor(d1);
//        p2.setCaretaker(c1);
//        p2.setAddress("address");
//        p2.setBirthDate(Date.valueOf(date));
//        p2.setGender(Gender.GENDER_M);
//        p2.setName("patient2");
//        p2.setPassword("patient2");
//        p2.setUsername("patient2");
//        p2.setRole(RoleName.ROLE_PATIENT);
//
//        Patient p3 = new Patient();
//        p3.setDoctor(d1);
//        p3.setCaretaker(c2);
//        p3.setAddress("address");
//        p3.setBirthDate(Date.valueOf(date));
//        p3.setGender(Gender.GENDER_F);
//        p3.setName("patient3");
//        p3.setPassword("patient3");
//        p3.setUsername("patient3");
//        p3.setRole(RoleName.ROLE_PATIENT);
//
//        Patient p4 = new Patient();
//        p4.setDoctor(d1);
//        p4.setCaretaker(c2);
//        p4.setAddress("address");
//        p4.setBirthDate(Date.valueOf(date));
//        p4.setGender(Gender.GENDER_M);
//        p4.setName("patient4");
//        p4.setPassword("patient4");
//        p4.setUsername("patient14");
//        p4.setRole(RoleName.ROLE_PATIENT);
//
//        c1.addPatient(p1);
//        c1.addPatient(p2);
//
//        c2.addPatient(p3);
//        c2.addPatient(p4);
//
//        d1.addPatient(p1);
//        d1.addPatient(p2);
//        d1.addPatient(p3);
//        d1.addPatient(p4);
//
//        doctorRepository.save(d1);
//
//        caretakerRepository.save(c1);
//        caretakerRepository.save(c2);
//
//        patientRepository.save(p1);
//        patientRepository.save(p2);
//        patientRepository.save(p3);
//        patientRepository.save(p4);
//
//        List<Patient> patients = caretakerService.getAllPatientsOfCaretaker("caretaker1");
//        for (Patient p : patients) {
//            System.out.println(p.getUsername());
//        }

//        Caretaker c = caretakerRepository.findByUsername("caretaker2");
//        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//        c.setPassword(encoder.encode("caretaker2"));
//        caretakerRepository.save(c);
//
//        Doctor d = doctorRepository.findByUsername("doctor1");
//        d.setPassword(encoder.encode("doctor1"));
//        doctorRepository.save(d);
//
//        Patient p1 = patientRepository.findByUsername("patient1");
//        p1.setPassword(encoder.encode("patient1"));
//        patientRepository.save(p1);
//
//        Patient p2 = patientRepository.findByUsername("patient2");
//        p2.setPassword(encoder.encode("patient2"));
//        patientRepository.save(p2);
//
//        Patient p3 = patientRepository.findByUsername("patient3");
//        p3.setPassword(encoder.encode("patient3"));
//        patientRepository.save(p3);
//
//        Patient p4 = patientRepository.findByUsername("patient14");
//        p4.setPassword(encoder.encode("patient4"));
//        patientRepository.save(p4);
    }
}

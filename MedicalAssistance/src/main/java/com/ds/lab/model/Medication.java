package com.ds.lab.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Medication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    private String sideEffects;
    private int intakePerDay;

    @ManyToMany(mappedBy = "medications")
    private Set<MedicationPlan> medicationPlans = new HashSet<>();

    public Medication() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public int getIntakePerDay() {
        return intakePerDay;
    }

    public void setIntakePerDay(int intakePerDay) {
        this.intakePerDay = intakePerDay;
    }

    public Set<MedicationPlan> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(Set<MedicationPlan> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }

    public void addMedicationPlan(MedicationPlan medicationPlan) {
        medicationPlans.add(medicationPlan);
        medicationPlan.getMedications().add(this);
    }

    public void removeMedicationPlan(MedicationPlan medicationPlan) {
        medicationPlans.add(medicationPlan);
        medicationPlan.getMedications().remove(this);
    }
}

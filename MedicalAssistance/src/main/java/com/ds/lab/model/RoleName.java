package com.ds.lab.model;

public enum RoleName {

    ROLE_DOCTOR,
    ROLE_PATIENT,
    ROLE_CARETAKER
}

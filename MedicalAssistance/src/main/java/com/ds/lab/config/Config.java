package com.ds.lab.config;

import com.ds.lab.service.MedicationService;
import com.ds.lab.service.PatientService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.remoting.rmi.RmiServiceExporter;

@Configuration
public class Config {

    private final PatientService patientService;

    public Config(PatientService patientService) {
        this.patientService = patientService;
    }

    @Bean
    MedicationService medicationService() {
        return patientService;
    }

    @Bean
    RmiServiceExporter exporter(MedicationService medicationService) {
        Class<MedicationService> serviceInterface = MedicationService.class;
        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setServiceInterface(serviceInterface);
        exporter.setService(medicationService);
        exporter.setServiceName(serviceInterface.getSimpleName());
        exporter.setRegistryPort(1099);
        return exporter;
    }

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        // this is the package name specified in the <generatePackage> specified in
        // pom.xml
        marshaller.setContextPath("com.ds.lab.soap");
        return marshaller;
    }

    @Bean
    public SOAPConnector soapConnector(Jaxb2Marshaller marshaller) {
        SOAPConnector client = new SOAPConnector();
        client.setDefaultUri("http://localhost:8000/ws/activities");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}
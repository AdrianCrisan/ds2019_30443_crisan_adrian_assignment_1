package com.ds.lab.config.service;

import com.ds.lab.repository.CaretakerRepository;
import com.ds.lab.repository.DoctorRepository;
import com.ds.lab.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    PatientRepository patientRepository;

    @Autowired
    CaretakerRepository caretakerRepository;

    @Autowired
    DoctorRepository doctorRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        if (doctorRepository.findByUsername(username) != null) {
            return UserPrinciple.build(doctorRepository.findByUsername(username));
        } else if (patientRepository.findByUsername(username) != null) {
            return UserPrinciple.build(patientRepository.findByUsername(username));
        } else if (caretakerRepository.findByUsername(username) != null) {
            return UserPrinciple.build(caretakerRepository.findByUsername(username));
        } else {
            throw new UsernameNotFoundException("User not found: " + username);
        }
    }
}

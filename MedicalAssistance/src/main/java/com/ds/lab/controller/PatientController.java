package com.ds.lab.controller;

import com.ds.lab.config.SOAPConnector;
import com.ds.lab.model.dto.PatientDTO;
import com.ds.lab.service.PatientService;
import com.ds.lab.soap.Activity;
import com.ds.lab.soap.GetActivityRequest;
import com.ds.lab.soap.GetActivityResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api")
public class PatientController {

    @Autowired
    private PatientService patientService;

    @GetMapping(value = "/patient")
    public PatientDTO getPatientInfo(@RequestParam("user") String username) {
        return patientService.getPatientInfo(username);
    }


}

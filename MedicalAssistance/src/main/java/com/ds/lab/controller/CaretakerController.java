package com.ds.lab.controller;

import com.ds.lab.model.dto.PatientDTO;
import com.ds.lab.service.CaretakerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class CaretakerController {

    @Autowired
    private CaretakerService caretakerService;

    @GetMapping(value = "/caretaker/patients")
    public List<PatientDTO> getPatients(@RequestParam("user") String username) {
        return caretakerService.getAllPatients(username);
    }
}

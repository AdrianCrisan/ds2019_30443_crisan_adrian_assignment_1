import { Component, OnInit } from '@angular/core';
import {User} from '../services/user';
import {UserService} from '../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-update-caretaker',
  templateUrl: './update-caretaker.component.html',
  styleUrls: ['./update-caretaker.component.css']
})
export class UpdateCaretakerComponent implements OnInit {

  id: number;
  caretaker: User;
  submitted = false;

  constructor(private userService: UserService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.caretaker = new User();
    this.id = this.route.snapshot.params['id'];
    this.userService.getCaretaker(this.id).subscribe(data => this.caretaker = data);
  }

  updateCaretaker() {
    this.userService.updateCaretaker(this.id, this.caretaker)
      .subscribe(data => console.log(data));
    this.caretaker = new User();
    this.goToList();
  }

  onSubmit() {
    this.submitted = true;
    this.updateCaretaker();
  }

  goToList() {
    this.router.navigate(['doctor/caretakers']);
  }

}

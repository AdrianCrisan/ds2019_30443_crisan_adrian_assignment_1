import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCaretakerComponent } from './update-caretaker.component';

describe('UpdateCaretakerComponent', () => {
  let component: UpdateCaretakerComponent;
  let fixture: ComponentFixture<UpdateCaretakerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCaretakerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCaretakerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

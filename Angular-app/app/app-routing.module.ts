import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {DoctorComponent} from './doctor/doctor.component';
import {CaretakerComponent} from './caretaker/caretaker.component';
import {PatientComponent} from './patient/patient.component';
import {LoginComponent} from './login/login.component';
import {PatientListComponent} from './patient-list/patient-list.component';
import {CaretakerListComponent} from './caretaker-list/caretaker-list.component';
import {AddPatientComponent} from './add-patient/add-patient.component';
import {UpdatePatientComponent} from './update-patient/update-patient.component';
import {AuthGuardService} from './auth/auth-guard.service';
import {AddCaretakerComponent} from './add-caretaker/add-caretaker.component';
import {UpdateCaretakerComponent} from './update-caretaker/update-caretaker.component';
import {AddMedicationPlanComponent} from './add-medication-plan/add-medication-plan.component';
import {MedicationListComponent} from './medication-list/medication-list.component';
import {AddMedicationComponent} from './add-medication/add-medication.component';
import {PatientActivityComponent} from "./patient-activity/patient-activity.component";


const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'doctor',
    component: DoctorComponent,
    canActivate: [AuthGuardService],
    data: {role: 'ROLE_DOCTOR'}
  },
  {
    path: 'caretaker/patients',
    component: CaretakerComponent,
    canActivate: [AuthGuardService],
    data: {role: 'ROLE_CARETAKER'}
  },
  {
    path: 'patient',
    component: PatientComponent,
    canActivate: [AuthGuardService],
    data: {role: 'ROLE_PATIENT'}
  },
  {
    path: 'doctor/patients',
    component: PatientListComponent,
    canActivate: [AuthGuardService],
    data: {role: 'ROLE_DOCTOR'}
  },
  {
    path: 'doctor/patients/add',
    component: AddPatientComponent,
    canActivate: [AuthGuardService],
    data: {role: 'ROLE_DOCTOR'}
  },
  {
    path: 'doctor/patients/update/:id',
    component: UpdatePatientComponent,
    canActivate: [AuthGuardService],
    data: {role: 'ROLE_DOCTOR'}
  },
  {
    path: 'doctor/caretakers',
    component: CaretakerListComponent,
    canActivate: [AuthGuardService],
    data: {role: 'ROLE_DOCTOR'}
  },
  {
    path: 'doctor/caretakers/add',
    component: AddCaretakerComponent,
    canActivate: [AuthGuardService],
    data: {role: 'ROLE_DOCTOR'}
  },
  {
    path: 'doctor/caretakers/update/:id',
    component: UpdateCaretakerComponent,
    canActivate: [AuthGuardService],
    data: {role: 'ROLE_DOCTOR'}
  },
  {
    path: 'doctor/medication/plan/:id',
    component: AddMedicationPlanComponent,
    canActivate: [AuthGuardService],
    data: {role: 'ROLE_DOCTOR'}
  },
  {
    path: 'doctor/medication',
    component: MedicationListComponent,
    canActivate: [AuthGuardService],
    data: {role: 'ROLE_DOCTOR'}
  },
  {
    path: 'doctor/medication/add',
    component: AddMedicationComponent,
    canActivate: [AuthGuardService],
    data: {role: 'ROLE_DOCTOR'}
  },
  {
    path: 'doctor/:id',
    component: PatientActivityComponent,
    canActivate: [AuthGuardService],
    data: {role: 'ROLE_DOCTOR'}
  },
  {
    path: 'auth/login',
    component: LoginComponent
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

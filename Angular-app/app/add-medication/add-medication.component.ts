import { Component, OnInit } from '@angular/core';
import {Medication} from '../services/medication';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-medication',
  templateUrl: './add-medication.component.html',
  styleUrls: ['./add-medication.component.css']
})
export class AddMedicationComponent implements OnInit {

  medication: Medication = new Medication();
  submitted = false;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
  }

  save() {
    this.userService.addMedication(this.medication)
      .subscribe(data => console.log(data),
        error => {
          console.log(error);
        });
    this.medication = new Medication();
    this.goToList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  goToList() {
    this.router.navigate(['doctor/medication']);
  }

}

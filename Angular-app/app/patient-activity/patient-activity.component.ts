import {Component, OnInit} from '@angular/core';
import {UserService} from '../services/user.service';
import {Activity} from '../services/activity';
import {ChartOptions, ChartType} from "chart.js";
import {Label} from "ng2-charts";
import * as pluginDataLabels from 'chart.js';

@Component({
  selector: 'app-patient-activity',
  templateUrl: './patient-activity.component.html',
  styleUrls: ['./patient-activity.component.css']
})
export class PatientActivityComponent implements OnInit {

  activities: Activity[];
  dailyActivities: Activity[];
  days: Array<string> = ['2011-11-28',
    '2011-11-29',
    '2011-11-30',
    '2011-12-01',
    '2011-12-02',
    '2011-12-03',
    '2011-12-04',
    '2011-12-05',
    '2011-12-06',
    '2011-12-07',
    '2011-12-08',
    '2011-12-09',
    '2011-12-10',
    '2011-12-11'];
  selectedDay: string;
  check = false;

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };

  public pieChartLabels: Label[] = ['Sleeping', 'Toileting', 'Showering', 'Breakfast', 'Grooming', 'Spare Time/TV', 'Snack', 'Lunch', 'Leaving'];
  public pieChartData: number[] = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [pluginDataLabels];

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.userService.getActivities().subscribe(
      data => {
        this.activities = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  generateChart(day: string) {
    this.pieChartData = [];
    this.dailyActivities = [];
    this.check = true;
    let total = 0, sleep = 0, toilet = 0, shower = 0, breakfast = 0, groom = 0, spare = 0, snack = 0, lunch = 0, leave = 0;
    this.activities.forEach(a => {
      if (a.day === day) {
        total = total + a.duration;
        if (a.type === 'Sleeping') {
          sleep = sleep + a.duration;
        }
        if (a.type === 'Toileting') {
          toilet = toilet + a.duration;
        }
        if (a.type === 'Showering') {
          shower = shower + a.duration;
        }
        if (a.type === 'Breakfast') {
          breakfast = breakfast + a.duration;
        }
        if (a.type === 'Grooming') {
          groom = groom + a.duration;
        }
        if (a.type === 'Spare_Time/TV') {
          spare = spare + a.duration;
        }
        if (a.type === 'Snack') {
          snack = snack + a.duration;
        }
        if (a.type === 'Lunch') {
          lunch = lunch + a.duration;
        }
        if (a.type === 'Leaving') {
          leave = leave + a.duration;
        }
      }
    });
    this.pieChartData.push(sleep, toilet, shower, breakfast, groom, spare, snack, lunch, leave);

    this.activities.forEach(a => {
      if (a.day === day) {
        this.dailyActivities.push(a);
      }
    });
    console.log(this.pieChartData);
  }

  sendActivity(a: Activity) {
    this.userService.setRecommendation('3', a).subscribe(
      data => console.log(data),
      error => console.log(error)
    );
  }

}

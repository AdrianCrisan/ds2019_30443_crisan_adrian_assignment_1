import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css']
})
export class DoctorComponent implements OnInit {


  constructor(private router: Router) { }

  ngOnInit() {
  }

  toPatients() {
    this.router.navigate(['doctor/patients']);
  }

  toCaretakers() {
    this.router.navigate(['doctor/caretakers']);
  }

  toMedication() {
    this.router.navigate(['doctor/medication']);
  }

  toCharts() {
    this.router.navigate(['doctor/3']);
  }
}

import { Component, OnInit } from '@angular/core';
import {User} from '../services/user';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../auth/token-storage.service';
import {Observable} from 'rxjs/index';

@Component({
  selector: 'app-add-patient',
  templateUrl: './add-patient.component.html',
  styleUrls: ['./add-patient.component.css']
})
export class AddPatientComponent implements OnInit {

  patient: User = new User();
  submitted = false;
  caretakers: Observable<any>;

  constructor(private userService: UserService, private router: Router, private tokenService: TokenStorageService) { }

  ngOnInit() {
    this.userService.getCaretakersOfDoctor(this.tokenService.getUsername()).subscribe(
      data => {
        this.caretakers = data;
      }
    );
  }

  save() {
    this.userService.addPatient(this.patient, this.tokenService.getUsername())
      .subscribe(data => console.log(data),
        error => {
          console.log(error);
        });
    this.patient = new User();
    this.goToList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  goToList() {
    this.router.navigate(['doctor/patients']);
  }

}

import { Component, OnInit } from '@angular/core';
import {User} from '../services/user';
import {UserService} from '../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-update-patient',
  templateUrl: './update-patient.component.html',
  styleUrls: ['./update-patient.component.css']
})
export class UpdatePatientComponent implements OnInit {

  id: number;
  patient: User;
  submitted = false;

  constructor(private userService: UserService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.patient = new User();
    this.id = this.route.snapshot.params['id'];
    this.userService.getPatient(this.id).subscribe(data => this.patient = data);
  }

  updatePatient() {
    this.userService.updatePatient(this.id, this.patient)
      .subscribe(data => console.log(data));
    this.patient = new User();
    this.goToList();
  }

  onSubmit() {
    this.submitted = true;
    this.updatePatient();
  }

  goToList() {
    this.router.navigate(['doctor/patients']);
  }

}

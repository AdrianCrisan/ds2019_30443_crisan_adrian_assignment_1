import { Component, OnInit } from '@angular/core';
import {User} from '../services/user';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../auth/token-storage.service';
import {Observable} from 'rxjs/index';

@Component({
  selector: 'app-add-caretaker',
  templateUrl: './add-caretaker.component.html',
  styleUrls: ['./add-caretaker.component.css']
})
export class AddCaretakerComponent implements OnInit {

  caretaker: User = new User();
  submitted = false;

  constructor(private userService: UserService, private router: Router, private tokenService: TokenStorageService) { }

  ngOnInit() {
  }

  save() {
    this.userService.addCaretaker(this.caretaker, this.tokenService.getUsername())
      .subscribe(data => console.log(data),
        error => {
          console.log(error);
        });
    this.caretaker = new User();
    this.goToList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  goToList() {
    this.router.navigate(['doctor/caretakers']);
  }

}

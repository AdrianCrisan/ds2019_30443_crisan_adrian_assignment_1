import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCaretakerComponent } from './add-caretaker.component';

describe('AddCaretakerComponent', () => {
  let component: AddCaretakerComponent;
  let fixture: ComponentFixture<AddCaretakerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCaretakerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCaretakerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

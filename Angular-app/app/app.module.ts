import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { DoctorComponent } from './doctor/doctor.component';
import { CaretakerComponent } from './caretaker/caretaker.component';
import { PatientComponent } from './patient/patient.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { httpInterceptorProviders } from './auth/auth-interceptor';
import { PatientListComponent } from './patient-list/patient-list.component';
import { CaretakerListComponent } from './caretaker-list/caretaker-list.component';
import { AddPatientComponent } from './add-patient/add-patient.component';
import { UpdatePatientComponent } from './update-patient/update-patient.component';
import {AuthGuardService} from './auth/auth-guard.service';
import {
  MatDatepickerModule, MatFormFieldModule, MatInputModule, MatNativeDateModule,
  MatSelectModule
} from '@angular/material';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import { AddCaretakerComponent } from './add-caretaker/add-caretaker.component';
import { UpdateCaretakerComponent } from './update-caretaker/update-caretaker.component';
import { AddMedicationPlanComponent } from './add-medication-plan/add-medication-plan.component';
import {NgSelectModule} from '@ng-select/ng-select';
import { MedicationListComponent } from './medication-list/medication-list.component';
import { AddMedicationComponent } from './add-medication/add-medication.component';
import { PatientActivityComponent } from './patient-activity/patient-activity.component';
import {ChartsModule} from 'ng2-charts';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    DoctorComponent,
    CaretakerComponent,
    PatientComponent,
    PatientListComponent,
    CaretakerListComponent,
    AddPatientComponent,
    UpdatePatientComponent,
    AddCaretakerComponent,
    UpdateCaretakerComponent,
    AddMedicationPlanComponent,
    MedicationListComponent,
    AddMedicationComponent,
    PatientActivityComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgSelectModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatSelectModule,
    MatInputModule,
    NoopAnimationsModule,
    ChartsModule
  ],
  providers: [httpInterceptorProviders, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }

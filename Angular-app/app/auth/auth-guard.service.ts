import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {TokenStorageService} from './token-storage.service';
import {Observable} from 'rxjs/index';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router, private tokenService: TokenStorageService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      const roles = this.tokenService.getAuthorities();
      if (roles.pop().toString() !== route.data.role.toString()) {
        this.router.navigate(['/auth/login']);
        return false;
      }

      return true;
  }
}

import { Component, OnInit } from '@angular/core';
import {MedicationPlan} from '../services/medication-plan';
import {UserService} from '../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Medication} from '../services/medication';

@Component({
  selector: 'app-add-medication-plan',
  templateUrl: './add-medication-plan.component.html',
  styleUrls: ['./add-medication-plan.component.css']
})
export class AddMedicationPlanComponent implements OnInit {

  medicationPlan: MedicationPlan = new MedicationPlan();
  medications: Medication[];
  selectedMedications: string[];
  id: string;
  submitted = false;

  constructor(private userService: UserService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.userService.getAllMedication().subscribe(
      data => {
        this.medications = data;
      }
    );
    this.id = this.route.snapshot.params['id'];
  }

  save() {
    this.medicationPlan.medications = this.selectedMedications;
    this.userService.addMedicationPlan(this.medicationPlan, this.id).subscribe(
      data => console.log(data),
      error => console.log(error)
    );
    this.medicationPlan = new MedicationPlan();
    this.goToList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  goToList() {
    this.router.navigate(['doctor/patients']);
  }

}

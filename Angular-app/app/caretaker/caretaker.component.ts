import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/index';
import {User} from '../services/user';
import {UserService} from '../services/user.service';
import {TokenStorageService} from '../auth/token-storage.service';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';

@Component({
  selector: 'app-caretaker',
  templateUrl: './caretaker.component.html',
  styleUrls: ['./caretaker.component.css']
})
export class CaretakerComponent implements OnInit {

  patients: Observable<User[]>;
  messageFromServer: string;

  private serverUrl = 'http://localhost:8082/message';
  private stompClient;

  constructor(private userService: UserService, private tokenService: TokenStorageService) {
    this.initializeWebSocketConnection();
  }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.patients = this.userService.getPatientsOfCaretaker(this.tokenService.getUsername());
  }

  initializeWebSocketConnection() {
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe('/topic', (message) => {
        if (message.body) {
          this.messageFromServer = message.body;
          console.log(message.body);
        }
      });
    });
  }
}

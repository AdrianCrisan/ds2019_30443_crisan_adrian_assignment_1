import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';
import {User} from '../services/user';
import {TokenStorageService} from '../auth/token-storage.service';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  patient: User;
  errorMessage: string;

  constructor(private userService: UserService, private tokenService: TokenStorageService) { }

  ngOnInit() {
    this.userService.getPatientByUsername(this.tokenService.getUsername()).subscribe(
      data => {
        //debugger;
        this.patient = data;
        if (this.patient.gender === 'GENDER_M') {
          this.patient.gender = 'MALE';
        } else {
          this.patient.gender = 'FEMALE';
        }
      },
      error => {
        this.errorMessage = `${error.status}: ${JSON.parse(error.error).message}`;
      }
    );
  }

}

import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/index';
import {User} from '../services/user';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../auth/token-storage.service';
import { WebSocketAPI } from '../services/WebSocketAPI';

@Component({
  selector: 'app-caretaker-list',
  templateUrl: './caretaker-list.component.html',
  styleUrls: ['./caretaker-list.component.css']
})
export class CaretakerListComponent implements OnInit {

  caretakers: Observable<User[]>;
  webSocketAPI: WebSocketAPI;
  message: any;

  constructor(private userService: UserService, private tokenService: TokenStorageService, private router: Router) { }

  ngOnInit() {
    this.reloadData();
    this.webSocketAPI = new WebSocketAPI();
    this.connect();
  }

  toAddCaretaker() {
    this.router.navigate(['doctor/caretakers/add']);
  }

  toUpdateCaretaker(id: number) {
    this.router.navigate(['doctor/caretakers/update', id]);
  }

  deleteCaretaker(id: number) {
    this.userService.deleteCaretaker(id)
      .subscribe(data => {
        console.log(data);
        this.reloadData();
      }, error => console.log(error));
  }

  reloadData() {
    this.caretakers = this.userService.getCaretakersOfDoctor(this.tokenService.getUsername());
  }

  connect(){
    this.webSocketAPI._connect();
  }

  disconnect(){
    this.webSocketAPI._disconnect();
  }

  handleMessage(message){
    this.message = message;
  }
}

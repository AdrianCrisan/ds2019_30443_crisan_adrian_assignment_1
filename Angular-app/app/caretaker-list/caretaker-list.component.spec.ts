import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaretakerListComponent } from './caretaker-list.component';

describe('CaretakerListComponent', () => {
  let component: CaretakerListComponent;
  let fixture: ComponentFixture<CaretakerListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaretakerListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaretakerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

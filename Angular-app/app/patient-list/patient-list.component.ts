import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/index';
import {User} from '../services/user';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../auth/token-storage.service';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {

  patients: Observable<User[]>;

  constructor(private userService: UserService, private tokenService: TokenStorageService, private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.patients = this.userService.getPatientsOfDoctor(this.tokenService.getUsername());
  }

  toAddPatient() {
    this.router.navigate(['doctor/patients/add']);
  }

  toUpdatePatient(id: number) {
    this.router.navigate(['doctor/patients/update', id]);
  }

  toCreateMedicationPlan(id: number) {
     this.router.navigate(['doctor/medication/plan', id]);
   }

  deletePatient(id: number) {
    this.userService.deletePatient(id)
      .subscribe(data => {
        console.log(data);
        this.reloadData();
      }, error => console.log(error));
  }
}

import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/index';
import {Medication} from '../services/medication';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-medication-list',
  templateUrl: './medication-list.component.html',
  styleUrls: ['./medication-list.component.css']
})
export class MedicationListComponent implements OnInit {

  medications: Observable<Medication[]>;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.medications = this.userService.getAllMedication();
  }

  toAddMedication() {
    this.router.navigate(['doctor/medication/add']);
  }

  deleteMedication(id: number) {
    this.userService.deleteMedication(id)
      .subscribe(data => {
        console.log(data);
        this.reloadData();
      }, error => console.log(error));
  }

}

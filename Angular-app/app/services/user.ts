import {MedicationPlan} from './medication-plan';

export class User {
  id: number;
  name: string;
  address: string;
  birthDate: string;
  gender: string;
  caretaker: string;
  medicationPlans: MedicationPlan[];
}

export class Activity {
  id: number;
  type: string;
  duration: number;
  day: string;
  recommend: string;
  normal: boolean;
}

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {Activity} from "./activity";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private doctorPatientsUrl = 'http://localhost:8080/api/doctor/patients';
  private doctorCaretakersUrl = 'http://localhost:8080/api/doctor/caretakers';
  private caretakerPatientsUrl = 'http://localhost:8080/api/caretaker/patients';
  private doctorMedicationUrl = 'http://localhost:8080/api/doctor/medication';
  private patientUrl = 'http://localhost:8080/api/patient';

  constructor(private http: HttpClient) { }

  getPatientsOfDoctor(username: string): Observable<any> {
    return this.http.get(this.doctorPatientsUrl, {params: {user: username}});
  }

  getPatientsOfCaretaker(username: string): Observable<any> {
    return this.http.get(this.caretakerPatientsUrl, {params: {user: username}});
  }

  getPatient(id: number): Observable<any> {
    return this.http.get(this.doctorPatientsUrl + `/${id}`);
  }

  getPatientByUsername(username: string): Observable<any> {
    return this.http.get(this.patientUrl, {params: {user: username}});
  }

  getCaretaker(id: number): Observable<any> {
    return this.http.get(this.doctorCaretakersUrl + `/${id}`);
  }

  addPatient(patient: object, username: string): Observable<object> {
      return this.http.post(this.doctorPatientsUrl, patient, {params: {doctor: username}});
  }

  updatePatient(id: number, patient: object): Observable<object> {
    return this.http.put(this.doctorPatientsUrl + `/update/${id}`, patient);
  }

  deletePatient(id: number): Observable<any> {
    return this.http.delete(this.doctorPatientsUrl + `/${id}`);
  }

  deleteCaretaker(id: number): Observable<any> {
    return this.http.delete(this.doctorCaretakersUrl + `/${id}`);
  }

  getCaretakersOfDoctor(username: string): Observable<any> {
    return this.http.get(this.doctorCaretakersUrl, {params: {user: username}});
  }

  addCaretaker(caretaker: object, username: string): Observable<object> {
    return this.http.post(this.doctorCaretakersUrl, caretaker, {params: {doctor: username}});
  }

  updateCaretaker(id: number, caretaker: object): Observable<object> {
    return this.http.put(this.doctorCaretakersUrl + `/update/${id}`, caretaker);
  }

  getAllMedication(): Observable<any> {
    return this.http.get(this.doctorMedicationUrl);
  }

  addMedicationPlan(medicationPlan: object, id: string): Observable<object> {
    return this.http.post(this.doctorMedicationUrl + `/plan`, medicationPlan, {params: {patientId: id}});
  }

  deleteMedication(id: number): Observable<any> {
    return this.http.delete(this.doctorMedicationUrl + `/${id}`);
  }

  addMedication(medication: object): Observable<object> {
    return this.http.post(this.doctorMedicationUrl, medication);
  }

  getActivities(): Observable<any> {
    return this.http.get('http://localhost:8080/api/doctor/3');
  }

  setRecommendation(id: string, a: Activity) {
    return this.http.post('http://localhost:8080/api/doctor/activity', a, {params: {patientId: id}});
  }

}

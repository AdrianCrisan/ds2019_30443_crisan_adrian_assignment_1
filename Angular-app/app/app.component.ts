import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from './auth/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private roles: string[];
  private authority: string;

  constructor(private tokenStorage: TokenStorageService) {}

  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
      this.roles.every(role => {
        if (role === 'ROLE_DOCTOR') {
          this.authority = 'doctor';
          return false;
        } else if (role === 'ROLE_CARETAKER') {
          this.authority = 'caretaker';
          return false;
        }
        this.authority = 'patient';
        return true;
      });
    }
  }
}
